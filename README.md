# README #
### ¿Para qué es este repositorio? ###

* Este repositorio contiene un trabajo práctico hecho para la materia Algoritmos y Programación III correspondiente a la Facultad de Ingeniería de la Universidad de Buenos Aires, durante el primer cuatrimestre de 2014
* La versión corresponde a la entrega realizada en la cursada.
* El objetivo del trabajo fue introducirnos hacia la programación orientada a objetos, evaluando el uso de conceptos tales como encapsulamiento, herencia, polimorfismo, delegación, etc.
* A su vez, se nos solicitó desarrollar bajo TDD y Extreme Programming.

### Setting ###

* El entorno utilizado para codificar el programa fue [Pharo 2.0](http://www.pharo.org/). En caso de usar el mismo, simplemente hay que abrir la VM de Pharo y arrastrar los archivos .st hacia ella, seleccionando la opción "Filein entire file".

### Who do I talk to? ###

* Enviar un inbox a https://bitbucket.org/lucasp90/